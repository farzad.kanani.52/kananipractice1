package online.world.kananikotlin

/*
print: 12345
       2345
       345
       45
       5
 */
fun main() {
    println(printNumber(5))
//    println(0..2)
}

var a = 1
fun printNumber(number: Int): Int {

    if (a != number) {

        for (b in a..number) {

            print(b)

        }
        println("")

        ++a
        printNumber(number)

    } else {

        return number

    }

    return printNumber(number)

}