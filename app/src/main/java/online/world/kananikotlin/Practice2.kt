package online.world.kananikotlin

var i = 0
var result = 1;

//Calculate the power of a number
fun main() {

    print("Enter Number Number: ")
    val num = readLine()?.toInt() ?: 0
    print("Enter PowerNumber: ")
    val pNum = readLine()?.toInt() ?: 0

    println(power(num, pNum))

}

fun power(number: Int, powerNum: Int): Int {

    if (i == powerNum) {

        return result

    } else {

        result *= number
        ++i
        power(number, powerNum)

    }

    return power(number, powerNum)


}